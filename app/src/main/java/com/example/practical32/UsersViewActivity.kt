package com.example.practical32

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.practical32.api.ApiService
import com.example.practical32.model.Users
import com.example.practical32.ui.theme.Practical32Theme
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UsersViewActivity : ComponentActivity() {

    lateinit var retrofit: Retrofit
    lateinit var service: ApiService
    val BASE_URL: String = "https://jsonplaceholder.typicode.com/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(ApiService::class.java)
        val call: Call<List<Users>> = service.getUserList()
        call.enqueue(object : Callback<List<Users>> {
            override fun onResponse(call: Call<List<Users>>, response: Response<List<Users>>) {
                if (response.isSuccessful) {
                    val res: ArrayList<Users> = (response.body() as ArrayList<Users>)
                    setContent {
                        Practical32Theme {
                            Surface(
                                modifier = Modifier.fillMaxSize(),
                                color = MaterialTheme.colors.background
                            ) {
                                val navController = rememberNavController()
                                NavHost(
                                    navController = navController,
                                    startDestination = "view_users",
                                    builder = {
                                        composable(
                                            "view_users",
                                            content = {
                                                ViewUsers(
                                                    res,
                                                    navController = navController
                                                )
                                            })
                                        composable(
                                            "remove_user",
                                            content = {
                                                RemoveUser(
                                                    res,
                                                    navController = navController
                                                )
                                            })
                                    })
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<Users>>, t: Throwable) {
                Toast
                    .makeText(
                        applicationContext,
                        "" + t.message,
                        Toast.LENGTH_SHORT
                    )
                    .show()
            }
        })
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ViewUsers(usersList: ArrayList<Users>, navController: NavHostController) {
    val context = LocalContext.current
    LazyColumn(contentPadding = PaddingValues(15.dp)) {

        itemsIndexed(usersList) { index, item ->
            Card(
                modifier = Modifier
                    .padding(8.dp, 4.dp)
                    .fillMaxWidth()
                    .combinedClickable (
                        onClick = {

                        },
                        onLongClick =  {
                            val builder = AlertDialog.Builder(context)
                            builder
                                .setMessage(
                                    "Do you want to remove " + usersList[index].name
                                            + " from list?"
                                )
                                .setCancelable(true)
                                .setPositiveButton("Yes") { _, _ ->
                                    usersList.removeAt(index)
                                    navController.navigate(route = "remove_user") {
                                        popUpTo(navController.graph.startDestinationId)
                                        launchSingleTop = true
                                    }
                                }
                                .setNegativeButton(
                                    "No"
                                ) { _, _ -> }
                            val alertDialog = builder.create()
                            alertDialog.setTitle("")
                            alertDialog.show()

                        }
                    ),
                shape = RoundedCornerShape(8.dp),
                elevation = 4.dp,
            ) {
                Surface {
                    Row(
                        Modifier
                            .padding(4.dp)
                            .fillMaxWidth()
                            /*.pointerInput(Unit) {
                                detectTapGestures(
                                    onPress = {},
                                    onLongPress =
                                )
                            }*/
                    ) {
                        Column(
                            modifier = Modifier
                                .weight(0.5f)
                                .padding(start = 5.dp)
                                .align(Alignment.CenterVertically)
                        )
                        {
                            Image(
                                painter = painterResource(
                                    id = R.drawable.ic_baseline_account_circle_24
                                ),
                                contentDescription = null
                            )
                        }
                        Column(
                            verticalArrangement = Arrangement.Center,
                            modifier = Modifier
                                .padding(4.dp)
                                .fillMaxWidth()
                                .weight(3f)
                        ) {
                            Text(
                                text = item.name,
                                style = MaterialTheme.typography.subtitle1,
                                fontWeight = FontWeight.Bold,
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                            Text(
                                text = item.email,
                                style = MaterialTheme.typography.subtitle2,
                                modifier = Modifier
                                    .background(
                                        Color(0, 0, 0, 15)
                                    )
                                    .padding(4.dp),
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                        }
                        Column(modifier = Modifier.weight(2f)) {
                            Image(
                                painter = painterResource(id = R.drawable.ic_baseline_location_on_24),
                                contentDescription = null,
                                Modifier.fillMaxWidth(),
                                alignment = Alignment.Center
                            )
                            Text(
                                modifier = Modifier.fillMaxWidth(),
                                text = item.address.city,
                                style = MaterialTheme.typography.body1,
                                textAlign = TextAlign.Center,
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                        }
                    }
                }
            }
        }
    }
}


@Composable
private fun RemoveUser(usersList: ArrayList<Users>, navController: NavHostController) {
    ViewUsers(usersList = usersList, navController = navController)
}
