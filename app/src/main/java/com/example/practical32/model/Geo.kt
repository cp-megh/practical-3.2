package com.example.practical32.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Geo(
    @SerializedName("lat")
    @Expose
    var lat: String,

    @SerializedName("lag")
    @Expose
    var lag: String
)
