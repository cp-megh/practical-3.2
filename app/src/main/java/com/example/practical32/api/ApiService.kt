package com.example.practical32.api

import com.example.practical32.model.Users
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("users")
    fun getUserList(): Call<List<Users>>
}